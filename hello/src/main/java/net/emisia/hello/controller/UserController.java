package net.emisia.hello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import net.emisia.hello.model.User;
import net.emisia.hello.service.UserService;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    private UserService getUserService() {
	return this.userService;
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User getUserById(@PathVariable Integer id) {
	return getUserService().findById(id);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Iterable<User> findAll() {
	return getUserService().findAll();
    }

    @RequestMapping(value = "/username/{name}", method = RequestMethod.GET)
    public Iterable<User> getUserByName(@PathVariable("name") String name) {
	return getUserService().findByUsername(name);
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public void createUser(@RequestBody User user) {
	getUserService().createUser(user);
    }

}
