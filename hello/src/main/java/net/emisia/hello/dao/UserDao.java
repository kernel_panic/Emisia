package net.emisia.hello.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.emisia.hello.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Integer> {
    public List<User> findBy_username(String name);
}
