package net.emisia.hello.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private int _id;

    @Column(nullable = false, unique = true)
    private String _username;

    @Column(nullable = false)
    private String _password;

    public int getId() {
	return _id;
    }

    public void setId(int id) {
	_id = id;
    }

    public String getUsername() {
	return _username;
    }

    public void setUsername(String username) {
	_username = username;
    }

    @JsonIgnore
    public String getPassword() {
	return _password;
    }

    @JsonProperty
    public void setPassword(String password) {
	_password = password;
    }
}
