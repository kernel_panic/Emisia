package net.emisia.hello.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import net.emisia.hello.dao.UserDao;
import net.emisia.hello.model.User;

@Service
public class UserService {

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Autowired
    private UserDao userDao;

    private UserDao getUserDao() {
	return userDao;
    }

    public User findById(Integer id) {
	return getUserDao().findById(id).get();
    }

    public Iterable<User> findByUsername(String n) {
	return getUserDao().findBy_username(n);
    }

    public Iterable<User> findAll() {
	return getUserDao().findAll();
    }

    public void createUser(User user) {
	user.setPassword(passwordEncoder.encode(user.getPassword()));
	getUserDao().save(user);
    }

}
